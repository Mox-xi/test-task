const image = require('./image');
const account = require('./account');


module.exports.router = function (req,res,data) {
    switch (req.regurl[0]) {
        case 'account':
            account.account(req,res,JSON.parse(data));
            break
        case 'image':
            image.image(req,res,data);
            break
        }
};
