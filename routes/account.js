const {reg,auth,login} = require('../actions/account');

module.exports.account = function(req,res,data){
    if(req.method === 'POST'){
        switch (req.regurl[1]) {
            case 'reg':
                reg(req,res,data).then(r => {
                    res.writeHead(200, 'OK', {
                        'Content-Type': 'application/json'
                    })
                    res.write(JSON.stringify(r));
                    res.end()
                })
                break;

            case 'login':
                login(data,res);
                break;

            case 'auth':
                auth(req,data,res)
                break
        }
    }
};