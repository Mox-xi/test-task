const http = require('http');
const fs = require('fs');
const router = require('./routes/index');


const server = http.createServer(function(req, res) {
    req.regurl = req.url.replace(/\//g,"\n").match(/.*./g)

    if(req.method === "GET") {
        fs.readFile(__dirname + req.url, (err, data) => {
            if (err) {
                console.log(err)
            }
            res.writeHead(200);
            res.end(data);
        });
    }

    if (req.method === 'POST') {
        req.on('data', function (data) {
            router.router(req,res,data)
        })}

});

server.listen(8085);
console.log("active ~ port 8085")


