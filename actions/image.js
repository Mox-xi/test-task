const fs = require('fs');

module.exports = {

    async set(data){
        try {
            let reg = data.toString().match(/.*/gm);
            let binSrc = data.slice(reg[0].length + reg[3].length + reg[6].length + 8,-48);
            const name = reg[3].match(/".+?"/g)[1].slice(1, -1);
            fs.writeFile('./image/' + name, binSrc, 'binary',(err)=>{
                console.log(err)
            });
            return {
                status: true,
                name: name
            }
        }
        catch (e) {
            return {
                error:e,
                status: false
            }
        }
    }

}