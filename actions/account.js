const redis = require('redis');
const client = redis.createClient();
const { promisify } = require("util");
const getAsync = promisify(client.get).bind(client);


module.exports = {

    async reg(req,res,data){
        if(!data.login || !data.password){
            return {
                status:false,
                exp: "missing params"
            }
        }

        client.set(data.login,JSON.stringify({
            login: data.login,
            password: data.password,
        }))

        return {
            login: data.login,
                status: true
        }

    },

    async login(data,res){

        if(!data.login || !data.password){
            res.end(JSON.stringify({
                status:false,
                exp: "missing params"
            }))
        }

        getAsync(data.login).then(r=>{
            let user = JSON.parse(r);
            if(user.login === data.login && user.password === data.password){
                let token = Math.random().toString(36).substring(2);
                res.writeHead(200, 'OK', {
                    'Set-Cookie': 'token='+token,
                    'Content-Type': 'application/json'
                })
                client.set(user.login, JSON.stringify({
                        login: user.login,
                        password: user.password,
                        token: token
                    })
                )
                res.end(JSON.stringify({
                    status: true
                }))
            }
            else{
                res.end(JSON.stringify({
                    status: false
                }))
            }
        })
    },

    async auth(req,data,res){
        getAsync(data.login).then(r => {
            let user = JSON.parse(r);
            let token = req.headers.cookie.match(/token=.*/).toString().replace('token=','');
            if(token === user.token){
                res.writeHead(200, 'OK', {'Content-Type': 'application/json'})
                res.write(JSON.stringify({
                        status: true
                    })
                )
                res.end()
            }
            else {
                res.writeHead(200, 'OK', {'Content-Type': 'application/json'})
                res.write(JSON.stringify({
                        status: false
                    })
                )
                res.end()
            }
        })
    },

};